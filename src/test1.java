import java.util.*;
import java.text.*;
import java.io.*;

public class test1 {
	
	public static void parse(String Cmd, HashMap<String, String> varChars){
		int DosVerMaj = 0;
		int DosVerMin = 2;
		int DosVerBuild = 2000;
		Scanner c = new Scanner(System.in);
		try{
			String[] Com = Cmd.split(" ");
			switch(Com[0]){
				case "hello":
					System.out.println("Hello World");
					break;
				case "pause":
					System.out.println("Press Enter...");
					c.nextLine();
					break;
				case "echo":
					String output = "";
					for(int i = 1; i < (Com.length); i++){
						output += Com[i] + " ";
					}
					System.out.println(output);
					break;
				case "time":
					Calendar timecal = Calendar.getInstance();
					SimpleDateFormat timesdf = new SimpleDateFormat("HH:mm:ss");
					System.out.println("The time is: " + timesdf.format(timecal.getTime()));
					break;
				case "date":
					Calendar datecal = Calendar.getInstance();
					SimpleDateFormat datesdf = new SimpleDateFormat("E, MMMM d, y");
					if (Com.length == 2){
						if (Com[1].equals("-e") || Com[1].equals("--era")){
							datesdf = new SimpleDateFormat("E, MMMM d, y G");
						} else if (Com[1].equals("-?") || Com[1].equals("--help")){
							System.out.println("Date");
							System.out.println("Swiches:");
							System.out.println("-? / --help | Displays this help command");
							System.out.println("-e / --era | Displays the designated Era");
							break;
						}
					}
						System.out.println("The date is: " + datesdf.format(datecal.getTime()));
						break;
					
				case "ver": //Displays the version of a specified software (JavaDos:jd, Operating system:OS) 
					switch(Com[1]){
					case "java":
						System.out.println(System.getProperty("java.version"));
						break;
					case "jd":
						System.out.println("Java DOS");
						System.out.println("Version " + Integer.toString(DosVerMaj) + "." + Integer.toString(DosVerMin) + "." + Integer.toString(DosVerBuild));
						break;
					case "os":
						// The key for getting operating system name
				        String name = "os.name";
				        // The key for getting operating system version
				        String version = "os.version";
				        // The key for getting operating system architecture
				        String architecture = "os.arch";
				        
				        System.out.println("Name   : " + System.getProperty(name));
				        System.out.println("Version: " + System.getProperty(version));
				        System.out.println("Arch   : " + System.getProperty(architecture));
				        break;
						
					}
					break;
				case "file":
				    
					switch(Com[1]){
					case "write":
						PrintWriter out = new PrintWriter(Com[2]);
						String iooutput = "";
						for(int i = 3; i < (Com.length); i++){
							iooutput += Com[i] + " ";
						}
						out.println(iooutput);
						out.close();
						break;
					// New features
					case "read":
						String fileName = Com[2];
						String line = null;
						
						try {
							FileReader fileReader = new FileReader(fileName);
							BufferedReader bufferedReader = new BufferedReader(fileReader);
							
							if (Com[3].equals(">")) {
								if (Com[4].contains("$") == false) {
									String filetovar = "";
									while((line = bufferedReader.readLine()) != null) {
										filetovar = line;
									}
									varChars.put(Com[4], line);
								} else {
									System.out.println("Varible name contains unwanted characters ($)");
								}
							} else {
							while((line = bufferedReader.readLine()) != null) {
								System.out.println(line);
							}
							}
							bufferedReader.close();
						}
						catch(FileNotFoundException ex) {
							System.out.println("Unable to open file '" + fileName + "'");
						}
						catch (IOException ex){
							System.out.println(
					                "Error reading file '" 
					                + fileName + "'"); 
						}
						break;
						}
						break;
			case "exec":
					if (Com[1].startsWith("$")) {
						parse(varChars.get(Com[1].replace("$", "")), varChars);
					} else {
					String fileName = Com[1];
					String line = null;
					
					try {
						FileReader fileReader = new FileReader(fileName);
						BufferedReader bufferedReader = new BufferedReader(fileReader);
						
						while((line = bufferedReader.readLine()) != null) {
							parse(line, varChars);
						}
						bufferedReader.close();
					}
					catch(FileNotFoundException ex) {
						System.out.println("Unable to open file '" + fileName + "'");
					}
					catch (IOException ex){
						System.out.println(
				                "Error reading file '" 
				                + fileName + "'"); 
					}
			}
					break;
			case "var":
				switch(Com[1]){
				case "get":
					if (varChars.containsKey(Com[2])){
						System.out.println(varChars.get(Com[2]));
					} else {
						System.out.println("Varible doesn't exist");
					}
					break;
				case "set":
					if (Com[2].contains("$") == false) {
					String varset = "";
					for(int i = 3; i < (Com.length); i++){
						varset += Com[i] + " ";
					}
					varChars.put(Com[2], varset);
					} else {
						throw new Exception("Varible name contains unwanted characters ($)");
					}
					break;
				case "input":
					System.out.print("?>");
					varChars.put(Com[2], c.nextLine());
					break;
					
				}
				break;
			case "exit":
					System.out.println("Goodbye!");
					System.exit(0);
				default:
					System.out.println("Invalid Command");
					int twen = 0;
				 
			}
			
			} catch (Exception ex){
			ex.printStackTrace();
		}
	}
	public static void main(String args[]){
		HashMap<String,String> varChars = new HashMap<String,String>();
		DosCMD dos = new DosCMD();
		Scanner c = new Scanner(System.in);
		System.out.println("Java DOS");
		while (true){
			//String x = c.nextLine();
			System.out.print(">");
			parse(c.nextLine(), varChars);
			
			}
		
	}
}



